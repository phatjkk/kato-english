import React, { useEffect } from "react";
import Home from "./pages/Home";
import Header from "./pages/Components/Header";
import Footer from "./pages/Components/Footer";

// const Header = React.lazy(() => import("./pages/Components/Header"));

const Learning = React.lazy(() => import("./pages/Learning"));
const Result = React.lazy(() => import("./pages/Result"));
const Profile = React.lazy(() => import("./pages/Profile"));
import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";
import { auth, provider, db } from "./firebase";
import  Reducer  from './GlobalState/Reducer';
import BeatLoader from "react-spinners/BeatLoader";
import './App.css'

function App() {
  const [isLogin,SetIsLogin] = React.useState(localStorage.getItem('katoAuth')!=="null")
  useEffect(()=>{
    auth.onAuthStateChanged(function (user) {
      if(!user){
        SetIsLogin(false)
        localStorage.setItem('katoAuth',"null")
      }
      else{
        SetIsLogin(true)
        localStorage.setItem('katoAuth',JSON.stringify(user))
      }
    });
  },[])
  return (
    <Reducer>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={
          <React.Suspense fallback={<LoadingPage />}>
            <HomePage />
          </React.Suspense>
        } />
        <Route path="learning" element={
          <React.Suspense fallback={<LoadingPage />}>
            {isLogin?<Learning />:<HomePage />}
          </React.Suspense>
        } />
        <Route path="profile" element={
          <React.Suspense fallback={<LoadingPage />}>
            {isLogin?<Profile />:<HomePage />}
          </React.Suspense>
        } />
        <Route path="result" element={
          <React.Suspense fallback={<LoadingPage />}>
            {isLogin?<Result />:<HomePage />}
          </React.Suspense>
        } />
      </Routes>
    </BrowserRouter>
    </Reducer>

  )
}
function HomePage(){
  return (
    <div className="App">
    <Header />
    <Home />
    <Footer />
  </div>
  )
}
function LoadingPage() {
  return (
    <div className="h-screen flex flex-col justify-center items-center">
      <BeatLoader color={"#00000"} loading={true} size={50} />
    </div>
  );
}

export default App
