import { ReactNode, useReducer, FC } from "react";
import Context, { InitStateGlobal } from "./Context";

const GlobalReducer = (state: any, action: any) => {
  switch (action.type) {
    case "SWITCH_THEME":
      return {
        ...state,
        ThemeMode: !state.ThemeMode,
      };
    case "SHOW_HIDE_LOGIN_BOX":
      return {
        ...state,
        isCloseLoginBox: !state.isCloseLoginBox,
      };
    case "SET_USER":
      if (action.value == null) {
        localStorage.setItem("katoAuth", "null");
      } else {
        localStorage.setItem("katoAuth", JSON.stringify(action.value));
      }

      return {
        ...state,
        currentUser: action.value,
      };
    case "SET_LEARN":
      return {
        ...state,
        currentLearn: action.value,
      };
    case "SET_CASE_ANSWER":
      return {
        ...state,
        caseAnswer: action.value,
      };
    case "SET_CHECK_CASE_CORRECT":
      return {
        ...state,
        checkCaseCorrect: action.value,
      };
    case "SET_NEXTBTN_SHOW":
      return {
        ...state,
        isNextButonShow: action.value,
      };
    case "SET_LESSON":
      return {
        ...state,
        lessonData: action.value,
      };

    case "SET_LEARNING_RESULT":
      return {
        ...state,
        learningResult: action.value,
      };
  }
};
// const [user, loading, error] = useAuthState(auth);
function Reducer({ children }: { children: ReactNode }) {
  //default lightTheme
  const [Action, Dispatch] = useReducer(GlobalReducer, InitStateGlobal);

  return (
    <Context.Provider value={[Action, Dispatch]}>{children}</Context.Provider>
  );
}

export default Reducer;
