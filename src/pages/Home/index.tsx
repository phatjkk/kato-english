import { useGlobalStore } from "../../GlobalState/";
import { useEffect, useState } from "react";
import { getCourse } from "../Learning/lib";
import BeatLoader from "react-spinners/BeatLoader";
import { Link, useNavigate } from "react-router-dom";
import { auth, db } from "../../firebase";
import { getDoc, doc } from "firebase/firestore";
import DATA_A1 from "../../data/a1.json";
import DATA_A2 from "../../data/a2.json";
import DATA_B1 from "../../data/b1.json";
import DATA_B2 from "../../data/b2.json";
import DATA_C1 from "../../data/c1.json";
import ProfileUI from "../Profile"
// import DATA_C2 from "../../data/c2.json";

interface MiniLesson {
  miniLessonName: string;
  lessonData: string[];
  wordNum: number;
  lessonNum:number;
  isLearned:boolean;
}
function Home() {
  const navigate = useNavigate();
  const [currentCourse, SetCC] = useState<any>(null);
  const [state, dispatch] = useGlobalStore();
  const [bg_color_from, Set_bg_color_from] = useState("");
  const [bg_color_to, Set_bg_color_to] = useState("");
  const [isWarningLogin, setIWL] = useState(false);
  const [Course_Data, setCourseData] = useState<any>(null);
  const [isLessonModalOpen, SetLessonModalOpen] = useState(false);
  const [currentClickedCourse,SetCurrentClickedCourse] = useState("");
  const colorsData = [
    {
      bg_color_from: "from-green-500",
      bg_color_to: "to-blue-500",
    },
    {
      bg_color_from: "from-blue-500",
      bg_color_to: "to-indigo-700",
    },
    {
      bg_color_from: "from-violet-800",
      bg_color_to: "to-pink-600",
    },
    {
      bg_color_from: "from-pink-600",
      bg_color_to: "to-red-600",
    },
    {
      bg_color_from: "from-red-600",
      bg_color_to: "to-orange-500",
    },
    {
      bg_color_from: "from-orange-500",
      bg_color_to: "to-yellow-500",
    },
    {
      bg_color_from: "from-yellow-500",
      bg_color_to: "to-pink-600",
    },
    {
      bg_color_from: "from-purple-500",
      bg_color_to: "to-blue-500",
    },
    {
      bg_color_from: "from-purple-500",
      bg_color_to: "to-pink-500",
    },
  ];
  function handleClickLesson(wordData,lessonNum) {
    dispatch({ type: "SET_LESSON", value: {data:wordData ,lessonNum:lessonNum,course:currentClickedCourse}});
    navigate("/learning", { replace: true });
  }
  async function handleClickCourse(courseKey) {
    SetCurrentClickedCourse(courseKey)
    const wordNumPerLesson = 10;
    if (state.currentUser == null) {
      setIWL(true);
      setTimeout(() => {
        setIWL(false);
      }, 3000);
    } else {
      let LessonDataLoaded: any = null;
      if (courseKey == "A1") {
        LessonDataLoaded = JSON.parse(JSON.stringify(DATA_A1));
      } else if (courseKey == "A2") {
        LessonDataLoaded = JSON.parse(JSON.stringify(DATA_A2));
      } else if (courseKey == "B1") {
        LessonDataLoaded = JSON.parse(JSON.stringify(DATA_B1));
      } else if (courseKey == "B2") {
        LessonDataLoaded = JSON.parse(JSON.stringify(DATA_B2));
      } else if (courseKey == "C1") {
        LessonDataLoaded = JSON.parse(JSON.stringify(DATA_C1));
      } else {
        return alert("Khoá học đang phát triển!");
      }
      const userDataCol = doc(db, "userdata", state.currentUser.uid);
      const docUserData = await getDoc(userDataCol);
      const userObjectData:any = docUserData.data() 
      const coursesLearned = userObjectData.learnedLessons["CEFR_" +courseKey ];

      const totalWordsCount = Object.keys(LessonDataLoaded).length;
      const lastMiniLessonCount = totalWordsCount % wordNumPerLesson;
      const totalMiniLessonCount =
        (totalWordsCount - lastMiniLessonCount) / wordNumPerLesson + 1;
      const arrayLesson: MiniLesson[] = [];

      for (let index = 0; index < totalMiniLessonCount; index++) {
        if (index === totalMiniLessonCount - 1) {
          const objMiniLesson: MiniLesson = {
            miniLessonName: "Bài " + (index + 1),
            lessonNum:index + 1,
            lessonData: LessonDataLoaded.slice(
              wordNumPerLesson * index,
              wordNumPerLesson * index + lastMiniLessonCount
            ),
            isLearned: isExitInArray(coursesLearned,index + 1),
            wordNum: lastMiniLessonCount
          };
          arrayLesson.push(objMiniLesson);
        } else {
          const objMiniLesson: MiniLesson = {
            miniLessonName: "Bài " + (index + 1),
            lessonNum:index + 1,
            lessonData: LessonDataLoaded.slice(
              wordNumPerLesson * index,
              wordNumPerLesson * index + wordNumPerLesson
            ),
            isLearned: isExitInArray(coursesLearned,index + 1),
            wordNum: wordNumPerLesson
          };
          arrayLesson.push(objMiniLesson);
        }
      }
      setCourseData(arrayLesson);
      SetLessonModalOpen(true);
      // dispatch({ type: "SET_LESSON", value: courseKey });
    }
  }
  function isExitInArray(arr,item){
    return arr.includes(item)
  }
  useEffect(() => {
    getCourse("courseData_CEFR").then((couserData) => {
      SetCC(couserData);
    });
  }, []);
  return (
    <div className={"  pb-5 " + (state.ThemeMode ? "" : "darkmode")}>
      <div
        className={
          (isWarningLogin ? "block" : "hidden") +
          " absolute right-0 bottom-2 alert w-96 alert-info  shadow-lg"
        }
      >
        <div>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            className="stroke-current flex-shrink-0 w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
            ></path>
          </svg>

          <span>
            Có vẻ như bạn chưa đăng nhập. Hãy đăng nhập để tiếp tục nhé!
          </span>
        </div>
      </div>
      <div className="flex font-['Nunito'] h-screen">
     

        <input type="checkbox" id="my-modal-3" className="modal-toggle" />
        <div className="modal">
          <div className="modal-box relative">
            <label
              htmlFor="my-modal-3"
              className="btn btn-sm btn-circle absolute right-2 top-2"
            >
              ✕
            </label>
            <ProfileUI />
          </div>
        </div>
        <div>
          <div>
            <div className="flex">
              <h3
                className={
                  "mt-[1.5em] ml-[2em]  text-xl font-bold " +
                  (state.ThemeMode ? "text-black" : "text-white")
                }
              >
                {/* 2000 Core English Words Vol.1 (A1-A2) */}
                CEFR Vocabulary Collection
              </h3>
              {/* <div className="dropdown mt-[1.5em] ml-[1em]">
              <label tabIndex={0} className="btn btn-info btn-sm ">
                Chọn khoá học
              </label>
              <ul
                tabIndex={0}
                className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52"
              >
                <li>
                  <a>Item 1</a>
                </li>
                <li>
                  <a>Item 2</a>
                </li>
              </ul>
            </div> */}
            </div>

            <div
              className={"modal " + (isLessonModalOpen ? "modal-open" : "")}
              id="my-modal-2"
            >
              <div className="modal-box relative">
                <label
                  htmlFor="my-modal-2"
                  onClick={() => SetLessonModalOpen(false)}
                  className="btn btn-sm btn-circle absolute right-2 top-2"
                >
                  ✕
                </label>
                <div className="overflow-x-auto">
                  <b>Từ vựng cấp độ {currentClickedCourse}</b>
                  <table className="mt-4 table w-full">
                    <thead>
                      <tr>
                        <th>Bài học</th>
                        <th>Lượng từ</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      {Course_Data == null
                        ? "Có lỗi khi load dữ liệu"
                        : // Object.keys(LessonDataLoaded).length
                          Course_Data.map((miniLesson) => (
                            <>
                              {/* wordNum:number; */}
                              <tr>
                                <td>{miniLesson.miniLessonName}</td>
                                <td>{miniLesson.wordNum}</td>
                                <td>
                                {
                                  miniLesson.isLearned?
                                  <button 
                                  onClick={() =>
                                    handleClickLesson(miniLesson.lessonData,miniLesson.lessonNum)
                                  }
                                  className="btn btn-warning btn-sm">Ôn tập</button>
                                  :
                                  <button
                                    onClick={() =>
                                      handleClickLesson(miniLesson.lessonData,miniLesson.lessonNum)
                                    }
                                    className="btn btn-info btn-sm"
                                  >
                                    Học ngay
                                  </button>
                                }
                                </td>
                              </tr>
                            </>
                          ))}
                      {/* <tr>
                      <td>Bài 1</td>
                      <td>10</td>
                      <td>
                        <button className="btn btn-warning btn-sm">Ôn tập</button>
                      </td>
                    </tr>
                    <tr>
                      <td>Bài 2</td>
                      <td>10</td>
                      <td>
                        <button className="btn btn-info btn-sm">Học ngay</button>
                      </td>
                    </tr>
                    <tr> */}
                    </tbody>
                  </table>
                </div>
                {/* <button
                  onClick={() => SetLessonModalOpen(false)}
                  className="btn"
                >
                  Đóng
                </button> */}
              </div>
            </div>

            <div className=" md:grid md:grid-cols-3 md:gap-3 text-white">
              {currentCourse !== null ? (
                Object.keys(currentCourse)
                  .sort()
                  .map((key: any, index: any) => (
                    // <Link key={index} to="/learning">
                    <button
                      key={index}
                      //update database xong moi sua handleClickUnit
                      onClick={() => {
                        handleClickCourse(key);
                      }}
                      className={
                        "w-[15em] h-[9em] transition hover:-translate-y-1 hover:scale-110 duration-300 ml-[2.5em] mt-[1.5em] bg-gradient-to-br" +
                        ` ${currentCourse[key].bg_color_from} ${currentCourse[key].bg_color_to} ` +
                        // " from-red-500 to-orange-600 " +
                        "rounded-3xl"
                      }
                    >
                      <p className="font-medium">Collection {index + 1}</p>
                      <h2 className="mb-2 font-bold text-lg">
                        {currentCourse[key].name}
                      </h2>
                      <p className="font-normal">{currentCourse[key].detail}</p>
                    </button>
                    // </Link>
                  ))
              ) : (
                <div className="h-screen w-screen flex justify-center items-center">
                  <div>
                    <BeatLoader color={"#3ABFF8"} loading={true} size={30} />
                  </div>
                </div>
              )}
            </div>
          </div>

          {/* <div>
          <h3 className="mt-[1.5em] ml-[2em] text-black text-lg font-bold">
            Lộ trình học trung cấp
          </h3>
          <div className="flex">
            <div className="ml-[2.5em] mt-[1.5em] p-10 bg-gradient-to-br from-violet-800 to-pink-600 rounded-3xl">
              <h2 className="mb-2 font-bold text-lg">
                Pimsleur English Course
              </h2>
              <p className="font-light">Người mới học hoàn toàn</p>
              <p className="font-light">Căn bản (~IELTS 0).</p>
            </div>
            <div className="ml-[2.5em] mt-[1.5em] p-10 bg-gradient-to-br from-pink-600 to-red-600 rounded-3xl">
              <h2 className="mb-2 font-bold text-lg">
                Original English Course
              </h2>
              <p className="font-light">Người đã có nền tảng căn bản.</p>
              <p className="font-light">Căn bản (~IELTS 0-3.0).</p>
            </div>
          </div>
        </div>
        <div>
          <h3 className="mt-[1.5em] ml-[2em] text-black text-lg font-bold">
            Lộ trình học nâng cao
          </h3>
          <div className="flex">
            <div className="ml-[2.5em] mt-[1.5em] p-10 bg-gradient-to-br from-green-500 to-blue-500 rounded-3xl">
              <h2 className="mb-2 font-bold text-lg">
                Pimsleur English Course
              </h2>
              <p className="font-light">Người mới học hoàn toàn</p>
              <p className="font-light">Căn bản (~IELTS 0).</p>
            </div>
            <div className="ml-[2.5em] mt-[1.5em] p-10 bg-gradient-to-br from-blue-500 to-indigo-700 rounded-3xl">
              <h2 className="mb-2 font-bold text-lg">
                Original English Course
              </h2>
              <p className="font-light">Người đã có nền tảng căn bản.</p>
              <p className="font-light">Căn bản (~IELTS 0-3.0).</p>
            </div>
          </div>
        </div> */}
        </div>
      </div>
    </div>
  );
}
export default Home;
