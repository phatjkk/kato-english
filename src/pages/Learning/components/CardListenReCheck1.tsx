import { useEffect } from "react";
import AUDIO_ICON from "../icon/audio.png";
import AUDIO_ICON_Playing from "../icon/audio-playing.gif";
import { useSpeechSynthesis } from "react-speech-kit";
import { useState } from "react";
import { useGlobalStore } from "../../../GlobalState/";
function CardListenReCheck1({ word,handleNext }) {
  const [state, dispatch] = useGlobalStore();
  const [isSpeakingWord, setISW] = useState(false);
  const { speak, voices } = useSpeechSynthesis({
    onEnd,
  });
  const handleKeypress = (e) => {
    //it triggers by pressing the enter key
    if (e.key === "Enter") {
        if(state.isNextButonShow){
          handleNext()
        }
    }
  };
  function handleAnswerInput(anwser){
    dispatch({type:"SET_CASE_ANSWER",value:anwser})
    if(anwser.length>0){
      dispatch({type:"SET_NEXTBTN_SHOW",value:true})
    }
    else{
      dispatch({type:"SET_NEXTBTN_SHOW",value:false})
    }
  }
  function onEnd() {
    setISW(false);
  }
  function handleSpeakWord() {
    setISW(true);
    const voice = voices.find((i) => i.lang === "en-US");
    speak({ text: word, voice });
  }
  useEffect(() => {
    dispatch({type:"SET_NEXTBTN_SHOW",value:false})
    // console.log(word, wordData)
    // handleSpeakWord()
  }, []);
  useEffect(() => {
    // doc khi load xong (mo lai khi done)
    if(voices.length>0){
        handleSpeakWord()
    }
}, [voices])
  return (
    <div className="card w-96 bg-base-100 shadow-xl">
      <div className="card-body">
        <button
          onClick={handleSpeakWord}
          className="btn btn-square btn-info btn-lg"
        >
          <img
            className="w-10 h-10"
            src={isSpeakingWord ? AUDIO_ICON_Playing : AUDIO_ICON}
            alt="fireSpot"
          />
          {/* <AUDIO_ICON/> */}
        </button>
        <h1 className="card-title">Nhập từ vựng mà bạn nghe được</h1>
        <input
          type="text"
          onKeyPress={handleKeypress}
          placeholder="Nhập tiếng Anh"
          autoFocus
          onChange={(e)=>{handleAnswerInput(e.target.value)}}
          className="input input-bordered input-info w-full max-w-xs"
        />
      </div>
    </div>
  );
}
export default CardListenReCheck1;
