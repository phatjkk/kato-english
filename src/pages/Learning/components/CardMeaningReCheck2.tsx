import { useEffect } from "react";
import { useState } from "react";
import { useGlobalStore } from "../../../GlobalState/";
function CardMeaningReCheck2({ word,wordData,handleNext }) {
  const [state, dispatch] = useGlobalStore();

  const handleKeypress = (e) => {
    //it triggers by pressing the enter key
    if (e.key === "Enter") {
      if(state.isNextButonShow){
        handleNext()
      }
  }
  };
  function handleAnswerInput(anwser){
    dispatch({type:"SET_CASE_ANSWER",value:anwser})
    if(anwser.length>0){
      dispatch({type:"SET_NEXTBTN_SHOW",value:true})
    }
    else{
      dispatch({type:"SET_NEXTBTN_SHOW",value:false})
    }
  }
  useEffect(() => {
    dispatch({type:"SET_NEXTBTN_SHOW",value:false})
    // console.log(word, wordData)
    // handleSpeakWord()
  }, []);
  return (
    <div className="card w-96 bg-base-100 shadow-xl">
      <div className="card-body">
        <h1 className="card-title">{String(wordData.meaning_vi)}</h1>
        <h1>Nghĩa Tiếng Anh của từ này là gì ?</h1>

        <input
          type="text"
          placeholder="Nhập tiếng Anh"
          onKeyPress={handleKeypress}
          autoFocus
          onChange={(e)=>{handleAnswerInput(e.target.value)}}
          className="input input-bordered input-info w-full max-w-xs"
        />
      </div>
    </div>
  );
}
export default CardMeaningReCheck2;
