import { useState, useEffect } from "react";
import { useGlobalStore } from "../../../GlobalState/";
import Card from "../components/Card";
import Result from "../../Result";
import CardListenReCheck1 from "../components/CardListenReCheck1";
import CardMeaningReCheck2 from "../components/CardMeaningReCheck2";
import BeatLoader from "react-spinners/BeatLoader";
import CorrentAlert from "../components/CorrentAlert";
import IncorrentAlert from "../components/IncorrectAlert";
import { Link, useNavigate } from "react-router-dom";
import CORRECT_SOUND from "../../../audio/correct.mp3";
import INCORRECT_SOUND from "../../../audio/incorrect.mp3";
import { auth, db } from "../../../firebase";
import {
  collection,
  setDoc,
  getDocs,
  getDoc,
  query,
  where,
  doc,
  updateDoc,
} from "firebase/firestore";
type TListIncorrect = {
  totalIncorrect: number;
  data: {
    check1: string[];
    check2: string[];
  };
};
type TWordData = {
  type: string;
  meaning_vi: string;
  meaning_en: string;
  ex: string;
};
function Ex1rmb() {
  const navigate = useNavigate();
  const [state, dispatch] = useGlobalStore();
  const [currentWord, SetWord] = useState("");
  const [currentWordData, SetWordData] = useState<TWordData>({
    type: "",
    meaning_vi: "",
    meaning_en: "",
    ex: "",
  });

  const [currentProcessBarValue, SetProcessBarValue] = useState(0);
  const [wordLearningIndex, SetWordLearningIndex] = useState(0);
  const [totalListItem, SetTotalListItem] = useState(0);
  const [wordsLeaningList, SetWordsLeaningList] = useState(
    state.currentLearn.data.map((data) => data.word)
  );
  const [learningCase, SetLearningCase] = useState("read");
  const [isAlert, SetIsAlert] = useState(false);
  const [isRePractice, SetIsRePractice] = useState(false);
  const [alertStatus, SetAlertStatus] = useState("correct");
  const [isNextButonLoad, SetNextButtonLoad] = useState(false);

  const [incorrectWordsList, SetIncorrectWordsList] = useState<TListIncorrect>({
    totalIncorrect: 0,
    data: {
      check1: [],
      check2: [],
    },
  });
  useEffect(() => {
    SetWord(wordsLeaningList[0]);
    SetWordDataByWord(wordsLeaningList[0]);
    SetTotalListItem(wordsLeaningList.length);
  }, []);
  useEffect(() => {
    SetWordDataByWord(currentWord);
  }, [currentWord]);
  function SetWordDataByWord(word) {
    SetWordData({
      type: "n",
      meaning_en: "loading..",
      meaning_vi: "loading..",
      ex: "loading",
    });
    word === ""
      ? ""
      : fetch(`/words/${word}.json`, {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (wordJsonData) {
            const meaning = wordJsonData.meaning[0];
            const type = meaning.pos.includes("noun")
              ? "n"
              : meaning.pos.includes("adj")
              ? "adj"
              : meaning.pos.includes("adv")
              ? "adv"
              : meaning.pos.includes("verb")
              ? "v"
              : "n";
            const meaning_en = meaning.en;
            const meaning_vi = meaning.vi;
            const ex = wordJsonData.sentences[0].en;
            SetWordData({
              type: type,
              meaning_en: meaning_en,
              meaning_vi: meaning_vi,
              ex: ex,
            });
          });
  }
  // const wordJsonData = require(`./../../../data/words/${word}.json`);
  function PlayCorrectAudio() {
    const audio = new Audio(CORRECT_SOUND);
    audio.play();
  }

  function PlayInCorrectAudio() {
    const audio = new Audio(INCORRECT_SOUND);
    audio.play();
  }
  function handleNextPracticeCase() {
    if (incorrectWordsList.data.check1.includes(currentWord)) {
      if (learningCase == "read") {
        SetLearningCase("check1");
      } else if (learningCase == "check1") {
        if (isAlert == false) {
          //check dap an
          if (state.caseAnswer == currentWord) {
            //dung
            PlayCorrectAudio();
            SetAlertStatus("correct");
            SetIncorrectWordsList((preList) => {
              return {
                ...preList,
                totalIncorrect: preList.totalIncorrect - 1,
                data: {
                  ...preList.data,
                  check1: preList.data.check1.filter((e) => e !== currentWord),
                },
              };
            });
          } else {
            //sai
            PlayInCorrectAudio();
            SetAlertStatus("incorrect");
            SetWordsLeaningList([...wordsLeaningList, currentWord]);
            SetTotalListItem(totalListItem + 1);
          }
          SetIsAlert(true);
        } else {
          // luc nay da xong task check 1
          SetWordLearningIndex(wordLearningIndex + 1);
          //xet truong hop xong practice test?
          const isDonePracticeTest = wordLearningIndex === totalListItem - 1;
          if (isDonePracticeTest) {
            handleDone();
          } else {
            SetWord(wordsLeaningList[wordLearningIndex + 1]);
            SetLearningCase("read");
            SetIsAlert(false);
          }
        }
      }
    } else if (incorrectWordsList.data.check2.includes(currentWord)) {
      if (learningCase == "check1") {
        SetLearningCase("read");
        SetIsAlert(false);
      } else if (learningCase === "read") {
        SetLearningCase("check2");
        SetIsAlert(false);
      } else if (learningCase === "check2") {
        if (isAlert == false) {
          //check dap an
          if (state.caseAnswer == currentWord) {
            //dung
            SetAlertStatus("correct");
            PlayCorrectAudio();
            SetIncorrectWordsList((preList) => {
              return {
                ...preList,
                totalIncorrect: preList.totalIncorrect - 1,
                data: {
                  ...preList.data,
                  check1: preList.data.check2.filter((e) => e !== currentWord),
                },
              };
            });
          } else {
            //sai
            PlayInCorrectAudio();
            SetAlertStatus("incorrect");
            SetWordsLeaningList([...wordsLeaningList, currentWord]);
            SetTotalListItem(totalListItem + 1);
          }
          SetIsAlert(true);
        } else {
          SetWordLearningIndex(wordLearningIndex + 1);
          //xet truong hop xong practice test?
          const isDonePracticeTest = wordLearningIndex === totalListItem - 1;
          if (isDonePracticeTest) {
            handleDone();
          } else {
            SetWord(wordsLeaningList[wordLearningIndex + 1]);
            SetLearningCase("read");
            SetIsAlert(false);
          }
        }
      }
    } else {
      handleDone();
    }
  }
  function handleNextLearnCase() {
    if (learningCase == "read") {
      SetLearningCase("check1");
    }
    if (learningCase == "check1") {
      if (isAlert == false) {
        //check dap an
        if (state.caseAnswer == currentWord) {
          //dung
          SetAlertStatus("correct");
          PlayCorrectAudio();
        } else {
          //sai
          SetIncorrectWordsList((preList) => {
            return {
              ...preList,
              totalIncorrect: preList.totalIncorrect + 1,
              data: {
                ...preList.data,
                check1: [...preList.data.check1, currentWord],
              },
            };
          });
          SetAlertStatus("incorrect");
          PlayInCorrectAudio();
        }

        SetIsAlert(true);
      } else {
        SetIsAlert(false);
        SetLearningCase("check2");
      }
    }
    if (learningCase == "check2") {
      if (isAlert == false) {
        //check dap an
        if (state.caseAnswer == currentWord) {
          //dung
          SetAlertStatus("correct");
          PlayCorrectAudio();
        } else {
          //sai
          SetIncorrectWordsList((preList) => {
            return {
              ...preList,
              totalIncorrect: preList.totalIncorrect + 1,
              data: {
                ...preList.data,
                check2: [...preList.data.check2, currentWord],
              },
            };
          });
          SetAlertStatus("incorrect");
          PlayInCorrectAudio();
        }
        SetIsAlert(true);
      } else {
        // ket thuc 1 tu vung, update process bar
        SetIsAlert(false);
        SetProcessBarValue(((wordLearningIndex + 1) * 100) / totalListItem);
        SetWordLearningIndex(wordLearningIndex + 1);
        const isDoneLearningCase = wordLearningIndex === totalListItem - 1;
        if (!isDoneLearningCase) {
          SetLearningCase("read");
          SetWord(wordsLeaningList[wordLearningIndex + 1]);
        } else {
          if (incorrectWordsList.totalIncorrect > 0) {
            //merge two array
            const rePracticeArr = [
              ...incorrectWordsList.data.check1,
              ...incorrectWordsList.data.check2,
            ];
            //tinh ket qua kha nang ghi nho
            const arrTuVungDaNho = wordsLeaningList.filter(
              (item) => !rePracticeArr.includes(item)
            ); //so da nho
            const kngh =
              (arrTuVungDaNho.length * 100) / wordsLeaningList.length;

            dispatch({
              type: "SET_LEARNING_RESULT",
              value: { wordsInput: wordsLeaningList.length, rmbPercent: kngh },
            });

            SetWordsLeaningList(rePracticeArr);
            SetWord(rePracticeArr[0]);
            SetLearningCase("read");
            SetIsAlert(false);
            SetWordLearningIndex(0);
            SetTotalListItem(rePracticeArr.length);
            SetIsRePractice(true);
          } else {
            dispatch({
              type: "SET_LEARNING_RESULT",
              value: { wordsInput: wordsLeaningList.length, rmbPercent: 100 },
            });
            handleDone();
          }
        }
      }
    }
  }
  async function handleDone() {
    if (state.currentUser != null) {
      (async () => {
        const q = query(
          collection(db, "userdata"),
          where("id", "==", state.currentUser.uid)
        );
        const querySnapshot = await getDocs(q);
        let foundItem: any = null;
        querySnapshot.forEach((doc) => {
          foundItem = doc.data();
        });

        if (foundItem === null) {
          const docRef = await setDoc(
            doc(collection(db, "userdata"), state.currentUser.uid),
            {
              learningExp: 0,
              streakDaysCountFrom: Date.now(),
              streakDaysLast: Date.now(),
              isDayStreak: false,
              id: state.currentUser.uid,
              learnedLessons: {
                CEFR_A1: [],
                CEFR_A2: [],
                CEFR_B1: [],
                CEFR_B2: [],
                CEFR_C1: [],
              },
              learnedWords: {
                Level_1: [],
                Level_2: [],
                Level_3: [],
                Level_4: [],
                Level_5: [],
              },
              recallWordTimer: [],
              friendsUid: [],
            }
          );
        }
        const wordsLearned = Array.from(
          state.currentLearn.data,
          (word: any) => {
            return { word: word.word.toString(), date: Date.now() };
          }
        );
        console.log(wordsLearned);
        const userDataCol = doc(db, "userdata", state.currentUser.uid);
        // const q2 = query(userDataCol, where("state", "==", "CA"), where("population", ">", 1000000));
        const docSnap = await getDoc(userDataCol);
        if (docSnap.exists()) {
          let userDataObject = docSnap.data();
          //tinh streak
          let dateStreakLast = userDataObject.streakDaysLast;
          let dateStreakFromCount = userDataObject.streakDaysCountFrom;

          const d = new Date(userDataObject.streakDaysLast);
          if (isUpdateStreak(d)) {
            dateStreakLast = Date.now();
          } else if (isToday(d)) {
          } else {
            dateStreakLast = Date.now();
            dateStreakFromCount = Date.now();
          }
          const courseObjName = "CEFR_" + state.currentLearn.course;
          let mergedLearnedWordsLV1 = state.currentLearn.data;
          let mergedWordTimers = wordsLearned;
          if (userDataObject.learnedWords.Level_1.length > 0) {
            let wordKeys = new Set(
              userDataObject.learnedWords.Level_1.map((w) => w.key)
            );
            mergedLearnedWordsLV1 = [
              ...userDataObject.learnedWords.Level_1,
              ...state.currentLearn.data.filter((d) => !wordKeys.has(d.key)),
            ];

            let wordTimers = new Set(wordsLearned.map((w) => w.word));
            mergedWordTimers = [
              ...userDataObject.recallWordTimer.filter(
                (d) => !wordTimers.has(d.word)
              ),
              ...wordsLearned,
            ];
          }
          const userDataObjectFinal = {
            learningExp: userDataObject.learningExp + 10,
            learnedWords: {
              ...userDataObject.learnedWords,
              Level_1: mergedLearnedWordsLV1,
            },
            learnedLessons: {
              ...userDataObject.learnedLessons,
              [courseObjName]: [
                ...new Set([
                  ...userDataObject.learnedLessons[courseObjName],
                  ...[state.currentLearn.lessonNumber],
                ]),
              ],
            },
            recallWordTimer: mergedWordTimers,
            streakDaysLast: dateStreakLast,
            streakDaysCountFrom: dateStreakFromCount,
          };
          await updateDoc(userDataCol, userDataObjectFinal);
        } else {
          console.log("No such document!");
        }
      })();
    }
    SetProcessBarValue(100);
    // return <Result />
    navigate("/result");
  }
  function isUpdateStreak(d) {
    let dn = new Date();
    let difference = dn.getTime() - d.getTime();
    let dayCheckYesterday = Math.ceil(difference / (1000 * 3600 * 24));
    if (dayCheckYesterday == 1) {
      return true;
    } else {
      return false;
    }
  }
  function isToday(date) {
    const today = new Date();

    if (today.toDateString() === date.toDateString()) {
      return true;
    }

    return false;
  }
  function handleNext() {
    SetNextButtonLoad(true);
    if (isRePractice) {
      handleNextPracticeCase();
    } else {
      handleNextLearnCase();
    }
    SetNextButtonLoad(false);
  }
  return (
    <>
      <div className="w-screen h-[4em] flex justify-center items-center">
        <Link to="/">
          <button className="btn btn-square btn-outline btn-error">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </button>
        </Link>
        <progress
          className="ml-5 progress progress-info h-2 w-[70%]"
          value={currentProcessBarValue}
          max="100"
        ></progress>
      </div>
      <div className="w-screen h-[100%] flex justify-center items-center">
        {currentWord == "" ||
        currentWord == undefined ||
        currentWord == null ? (
          <BeatLoader color={"#3ABFF8"} loading={true} size={30} />
        ) : (
          {
            read: (
              <Card
                word={currentWord}
                wordData={currentWordData}
                handleNext={handleNext}
              />
            ),

            check1: (
              <CardListenReCheck1 handleNext={handleNext} word={currentWord} />
            ),

            check2: (
              <CardMeaningReCheck2
                handleNext={handleNext}
                word={currentWord}
                wordData={currentWordData}
              />
            ),
          }[learningCase]
        )}
      </div>
      {isAlert ? (
        alertStatus == "correct" ? (
          <CorrentAlert word={currentWord} wordData={currentWordData} />
        ) : alertStatus == "incorrect" ? (
          <IncorrentAlert word={currentWord} wordData={currentWordData} />
        ) : (
          ""
        )
      ) : (
        ""
      )}
      <div className="absolute w-screen h-[7em] outline-gray-500 bottom-0 flex justify-center items-center">
        <button className="btn btn-lg btn-ghost">Bỏ qua</button>
        <button
          className={
            (state.isNextButonShow ? "" : "btn-disabled") +
            ` ml-[15%] btn-lg btn btn-info`
          }
          onClick={handleNext}
        >
          {isNextButonLoad ? (
            <BeatLoader color={"#ffffff"} loading={true} size={10} />
          ) : (
            "TIẾP TỤC"
          )}
        </button>
      </div>
    </>
  );
}
export default Ex1rmb;
