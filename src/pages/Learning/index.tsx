import { db } from "../../firebase";
import { doc, getDoc, collection } from "firebase/firestore";
import { useState, useEffect } from "react";
import { useGlobalStore } from "../../GlobalState/";
import Ex1rmb from "./exercises/Ex1rmb";
import { getWords } from "./lib";
function Learning() {
  const [state, dispatch] = useGlobalStore();
  const [isRememberWord, SetIsRMBW] = useState(false);
  useEffect(() => {
    // getWords(state.chooseLesson).then((worddb:any)=>{
    dispatch({
      type: "SET_LEARN",
      value: {
        typeEx: "Remember_Word",
        lessonNumber: state.lessonData.lessonNum,
        data: state.lessonData.data,
        course: state.lessonData.course
      },
    });
    SetIsRMBW(true);
    dispatch({ type: "SET_NEXTBTN_SHOW", value: true });
    // })
  }, []);

  return <div>{isRememberWord ? <Ex1rmb /> : ""}</div>;
}
export default Learning;
