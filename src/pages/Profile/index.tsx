import { useEffect, useState } from "react";
import { useGlobalStore } from "../../GlobalState/";
import IMG_avatar from "../icon/avatar_default.gif";
import { auth, db } from "../../firebase";
import { getDoc, doc } from "firebase/firestore";
type IWordsLevelCount = {
  level_1: number;
  level_2: number;
  level_3: number;
  level_4: number;
  level_5: number;
};

function Profile() {
  const [state, dispatch] = useGlobalStore();
  const [profileIMG, SetProfileIMG] = useState(IMG_avatar);
  const [profileMail, SetProfileMail] = useState("");
  const [maxWordLearned, SetMaxWordLearned] = useState(0);
  const [chartData, SetChartData] = useState<any>();
  const [registerDay, SetRegisterDay] = useState<string>("");

  const [wordsLevelChartData, SetWordLevelChartData] =
    useState<IWordsLevelCount>({
      level_1: 0,
      level_2: 0,
      level_3: 0,
      level_4: 0,
      level_5: 0,
    });
  const [wordsLevelCount, SetWordLevelCount] = useState<IWordsLevelCount>({
    level_1: 0,
    level_2: 0,
    level_3: 0,
    level_4: 0,
    level_5: 0,
  });
  useEffect(() => {
    if (state.currentUser != null) {
      SetProfileMail(state.currentUser.providerData[0].email);
      if (state.currentUser.providerData[0].photoURL === null) {
        SetProfileIMG(String(IMG_avatar));
      } else {
        SetProfileIMG(String(state.currentUser.photoURL));
      }
      (async () => {
        const userCol = doc(db, "users", state.currentUser.uid);
        const docUser = await getDoc(userCol);
        if (docUser.exists()) {
          let userObject = docUser.data();
          const DRegister = new Date(userObject.registerDay);
          const yyyy = DRegister.getFullYear();
          let mm: number = DRegister.getMonth() + 1; // Months start at 0!
          let dd: number = DRegister.getDate();
          SetRegisterDay(
            dd.toString() + "/" + mm.toString() + "/" + yyyy.toString()
          );
        }
        const userDataCol = doc(db, "userdata", state.currentUser.uid);
        const docUserData = await getDoc(userDataCol);
        if (docUserData.exists()) {
          let userDataObject = docUserData.data();
          let highestLevelWordCount = Math.max(
            userDataObject?.learnedWords.Level_1.length,
            userDataObject?.learnedWords.Level_2.length,
            userDataObject?.learnedWords.Level_3.length,
            userDataObject?.learnedWords.Level_4.length,
            userDataObject?.learnedWords.Level_5.length
          );
          SetWordLevelCount({
            level_1: userDataObject?.learnedWords.Level_1.length,
            level_2: userDataObject?.learnedWords.Level_2.length,
            level_3: userDataObject?.learnedWords.Level_3.length,
            level_4: userDataObject?.learnedWords.Level_4.length,
            level_5: userDataObject?.learnedWords.Level_5.length,
          });
          SetWordLevelChartData({
            level_1:
              (userDataObject?.learnedWords.Level_1.length * 100) /
              highestLevelWordCount,
            level_2:
              (userDataObject?.learnedWords.Level_2.length * 100) /
              highestLevelWordCount,
            level_3:
              (userDataObject?.learnedWords.Level_3.length * 100) /
              highestLevelWordCount,
            level_4:
              (userDataObject?.learnedWords.Level_4.length * 100) /
              highestLevelWordCount,
            level_5:
              (userDataObject?.learnedWords.Level_5.length * 100) /
              highestLevelWordCount,
          });
          SetChartData(chartData);
          // SetMaxWordLearned(maxWordCount);
        }
      })();
    }
  }, [state.currentUser]);
  return (
    <>
      <div className="col-span-3 row-span-3  flex flex-shrink-0 flex-col mx-0 w-full svelte-1n6ue57">
        <div className="">
          <div tabIndex={0} className="bg-opacity-100">
            <div className="tabs w-full flex-grow-0">
              <button className="tab tab-lifted tab-active tab-border-none tab-lg flex-1">
                Thành tích
              </button>
              <button className="tab tab-lifted tab-border-none tab-lg flex-1">
                Thông tin
              </button>
              <button className="tab tab-lifted tab-border-none tab-lg flex-1">
                Cài đặt
              </button>
            </div>
          </div>
        </div>
        <div className="bg-base-100 grid w-full flex-grow gap-3 rounded-xl rounded-tl-none p-6 shadow-xl">
          <div className="flex items-center space-x-2">
            <div className="">
              <div tabIndex={0}>
                <div className="online avatar">
                  <div className="mask mask-hexagon bg-base-content h-16 w-16 bg-opacity-10 p-px">
                    <img
                      src={profileIMG}
                      onError={({ currentTarget }) => {
                        currentTarget.onerror = null; // prevents looping
                        currentTarget.src = IMG_avatar;
                      }}
                      alt="Avatar Tailwind CSS Component"
                      className="mask mask-hexagon"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="text-lg font-extrabold">{profileMail}</div>
              <div className="text-base-content/70 text-sm">
                Đã tham gia vào ngày {registerDay}
              </div>
            </div>
          </div>
          <div>
            <div tabIndex={0}>
              <div className="divider text-base-content/60 m-0">
                Tổng kết thành tích
              </div>
            </div>
          </div>
          <div className="text-lg font-extrabold">Thành tích của bạn</div>
          <div className="grid gap-3">
            <div className="dropdown dropdown-top">
              <div tabIndex={0}>
              <div className="flex items-center p-1">
                  <span className="text-base-content/70 w-48 text-xs">
                    Từ vựng mới học
                  </span>{" "}
                  <div className="w-full bg-gray-200 rounded-full ">
                    <div
                      className="bg-red-600 text-xs font-medium text-blue-100 text-center p-0.5 leading-none rounded-full"
                      style={{ width: "100%" }}
                    >
                      {wordsLevelCount.level_1 + " từ"}
                    </div>
                  </div>
                </div>
                
                <div className="flex items-center p-1">
                  <span className="text-base-content/70 w-48 text-xs">
                    Từ vựng có thể quên
                  </span>{" "}
                  <div className="w-full bg-gray-200 rounded-full ">
                    <div
                      className="bg-orange-600 text-xs font-medium text-blue-100 text-center p-0.5 leading-none rounded-full"
                      style={{ width: "100%" }}
                    >
                      {wordsLevelCount.level_2 + " từ"}
                    </div>
                  </div>
                </div>
                <div className="flex items-center p-1">
                  <span className="text-base-content/70 w-48 text-xs">
                    Từ vựng đang nhớ
                  </span>{" "}
                  <div className="w-full bg-gray-200 rounded-full ">
                    <div
                      className="bg-yellow-600 text-xs font-medium text-blue-100 text-center p-0.5 leading-none rounded-full"
                      style={{ width: "100%" }}
                    >
                      {wordsLevelCount.level_3 + " từ"}
                    </div>
                  </div>
                </div>
                <div className="flex items-center p-1">
                  <span className="text-base-content/70 w-48 text-xs">
                    Từ vựng đã nhớ
                  </span>{" "}
                  <div className="w-full bg-gray-200 rounded-full ">
                    <div
                      className="bg-blue-600 text-xs font-medium text-blue-100 text-center p-0.5 leading-none rounded-full"
                      style={{ width:  "100%" }}
                    >
                      {wordsLevelCount.level_4 + " từ"}
                    </div>
                  </div>
                </div>
                <div className="flex items-center p-1">
                  <span className="text-base-content/70 w-48 text-xs">
                    Từ vựng đã thông thạo (thuộc lòng)
                  </span>{" "}
                  <div className="w-full bg-gray-200 rounded-full ">
                  
                    <div
                      className="bg-green-600 text-xs font-medium text-blue-100 text-center p-0.5 leading-none rounded-full"
                      style={{ width:  "100%" }}
                    >
                      {wordsLevelCount.level_5 + " từ"}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default Profile;
