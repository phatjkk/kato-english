import DoneTickPNG from "../icon/done-tick.png";
import { useGlobalStore } from "../../GlobalState/";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
function Result() {
    const navigate = useNavigate()
  const [state, dispatch] = useGlobalStore();
  const [learnedPercent, setLP] = useState(100);
  const [learnedWords, setLW] = useState(10);

  useEffect(() => {
    if (state.learningResult !== null) {
      setLP(state.learningResult.rmbPercent);
      setLW(state.learningResult.wordsInput);
    }
  }, []);
  const styleProcess = {
    "--value": learnedPercent,
    "--size": "7rem",
    "--thickness": "0.5rem",
  } as React.CSSProperties;
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <div className="card w-96 bg-base-100 shadow-xl">
        <div className="card-body">
          <img className=" h-20 w-20" src={DoneTickPNG || ""} />

          <h2 className="card-title">Đã hoàn thành bài học</h2>
          <p>
            Bạn thật xuất sắc, đây là kết quả phân tích khả năng ghi nhớ của
            bạn:
          </p>
          <div className="flex justify-center ">
            <div className="radial-progress text-success" style={styleProcess}>
              <b>{Math.round(learnedPercent * 10) / 10}%</b>
            </div>
          </div>
          <p>
            Bạn đã nạp tổng cộng <b className="text-blue-500">{learnedWords} từ vựng</b> vào bộ nhớ dài
            hạn của mình.
          </p>
          <div className="card-actions justify-end">
            <button className="btn btn-info" onClick={()=>{navigate('/')}}>Hoàn tất</button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Result;
